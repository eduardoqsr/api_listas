var indexSectionsWithContent =
{
  0: "acdfhilmnrst",
  1: "fl",
  2: "dl",
  3: "adfmnrt",
  4: "dfhint",
  5: "fnt",
  6: "cls"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Macros"
};

