#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "linkedList.h"
#include "dbg.h"

typedef struct form
{
	int ID;
	char * name;
}TestForm;

void testInts();


int main()
{
	printf("\n");
	testInts();
	printf("\n\n");
	
	return 0;
}

void testInts()
{
	List intList;
	int nums[3] = { 4, 9, 16 };
	int retrieved = 0;
	
	printf("-----Construyendo una nueva lista para INTs-----\n");
	newList(&intList, sizeof(int), NULL);
	
	printf("Agregando %d a la cabeza\n", nums[0]);
	addHead(&intList, &nums[0]);
	printf("Agregando %d en la cola\n", nums[1]);
	addTail(&intList, &nums[1]);
	printf("Agregando %d en la cola\n", nums[2]);
	addTail(&intList, &nums[2]);
	printf("-----Lista creada con éxito-----\n");
	
	retrieved = *(int *)retrieveTail(&intList);
	printf(": %d\n", retrieved);
	retrieved = *(int *)retrieveHead(&intList);
	printf(": %d\n", retrieved);
	removeTail(&intList);
	printf("Cola removida\n");
	retrieved = *(int *)retrieveTail(&intList);
	printf(": %d\n", retrieved);
	
	destroyList(&intList);
	printf("-----Lista destruida-----\n");
}

void freeStructs(void * data)
{
	TestForm * cast = (TestForm *)data;
	free(cast->name);	
}
