/**
* @file linkedList.h
* Cabecera del fichero listTest.c
* Archivo likedList que contiene las funciones para las listas enlazadas
* Contiene la estructura listNode
* Se declara un Node de tipo listNode
* 
* @author  EduardoQSR
* @date    18 - 04 - 2020
*/

#ifndef _list_h_
#define _list_h_

#include <stdio.h>
#include <stdlib.h>
#include "dbg.h"
#include <malloc.h>

typedef void (*freeData)(void *);

/*

*/
typedef struct listNode
{
	void * data;			// Puntero a los datos capturado por el nodo de lista
	struct listNode * next;		//Puntero al siguiente elemento en la lista
}Node;

typedef struct
{
	int dataSize;			// Tamaño de los datos almacenados en la lista
	Node * head; 			// Puntero para listar cabeza
	Node * tail;			// Puntero para listar cola
	freeData freeFunc;		// Puntero a la función que libera cualquier dato no almacenado directamente en el nodo de la lista
}List;

/**
 * @brief   newList
 * @param   *list - Puntero para que llame a la estructura de la lista
 * @param	dataSize - El tamaño en bytes de cada dato que se mantiene en la lista
 * @param	function - puntero a la función requerida para liberar datos que no se almacenan directamente en el nodo de la lista (puede ser NULL si no es necesario)
 * @return Devuelve 0 en caso de éxito, devuelve -1 en caso de error (tamaño de datos no natural)
 * @brief produce una lista que incluye las tareas especificadas
 */

int newList(List * list, int dataSize, freeData function);

/**
 * @brief	destroyList
 * @param	*list - pointer to an existing list
 * @param	devuelve 0 en caso de éxito devuelve -1 en caso de error (no se especifica una lista)
 * @return	libera toda la memoria asignada a los nodos de la lista, llama a la función para liberar datos si existe
 */

int destroyList(List * list);

/**
 * @brief	addHead
 * @param	*list - Puntero a una lista existente
 * @param	*data - Puntero a los datos que se almacenarán
 * @return	devuelve 0 en caso de éxito devuelve -1 en caso de error (sin lista especificada, error de memoria)
 * @brief	Asigna memoria para el nuevo nodo y la coloca al principio de la lista
 */

int addHead(List * list, void * data);

/**
 * @brief	addTail
 * @param	*list - puntero a una lista existente
 * @param	*data - puntero a los datos que se almacenarán
 * @return	devuelve 0 en caso de éxito devuelve -1 en caso de error (sin lista especificada, error de memoria)
 * @brief	Asigna memoria para el nuevo nodo y lo coloca al final de la lista
 */

int addTail(List * list, void * data);

/**
 * @brief	removeHead
 * @param	*list - puntero a una lista existente
 * @return	devuelve 0 en caso de éxito devuelve -1 en caso de error (sin lista especificada, lista vacía)
 * @brief	libera toda la memoria asignada para el nodo al principio de la lista, llama a la función para liberar datos si existe
 */

int removeHead(List * list);

/**
 * @brief removeTail
 * @param	*list - puntero a una lista existente
 * @return	devuelve 0 en caso de éxito devuelve -1 en caso de error (sin lista especificada, lista vacía)
 * @brief	libera toda la memoria asignada para el nodo en la cola de la lista, llama a la función para liberar datos si existe
 */

int removeTail(List * list);

/**
 * @brief	retrieveHead
 * @param	*list - puntero a una lista existente
 * @return	devuelve el puntero a los datos en caso de éxito devuelve NULL en caso de error (sin lista especificada, lista vacía)
 * @return	devuelve un puntero a los datos almacenados al principio de la lista
 */

void * retrieveHead(List * list);

/**
 * @brief	retrieveTail
 * @param	*list - puntero a una lista existente
 * @return	devuelve el puntero a los datos en caso de éxito devuelve NULL en caso de error (sin lista especificada, lista vacía)
 * @return	devuelve un puntero a los datos almacenados al final de la lista
 */

void * retrieveTail(List * list);

#endif
